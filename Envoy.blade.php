@servers(['localhost' => '127.0.0.1'])

@setup
    $repository = 'git@gitlab.com:serempre/test-laravel.git';
    $releases_dir = '/var/www/app/releases';
    $app_dir = '/var/www/app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    @if ($branch)
        git clone --branch {{ $branch }} --depth 1 {{ $repository }} {{ $new_release_dir }}
    @else
        git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    @endif
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o --no-dev
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env files'
    cp {{ $new_release_dir }}/.env.example {{ $new_release_dir }}/.env
    ln -nfs {{ $app_dir }}/.env.local {{ $new_release_dir }}/.env.local

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

    echo 'Make www-data the owner'
    chown -R www-data:www-data {{ $new_release_dir }}
    chown -R www-data:www-data {{ $app_dir }}/storage
@endtask
