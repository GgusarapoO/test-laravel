#!/usr/bin/env bash
while getopts i:p:k:e:c:f: flag
do
    case "${flag}" in
        i) ci_registry_image=${OPTARG};;
        p) project_key=${OPTARG};;
        k) server_key=${OPTARG};;
        e) environment=${OPTARG};;
        c) container_port=${OPTARG};;
        f) environment_file=${OPTARG};;
    esac
done

echo -e "Pulling image starting"
docker pull $ci_registry_image
echo -e "Checking if there are containers already running, is so, then stop and remove them"
EXISTING_CONTAINER_IDS=$(docker ps -a | grep "${project_key}_${environment}" | cut -d " " -f 1)
if [[ $EXISTING_CONTAINER_IDS ]]; then
    echo -e "Stopping ${EXISTING_CONTAINER_IDS}"
    docker stop "${EXISTING_CONTAINER_IDS}"
    echo -e "Removing ${EXISTING_CONTAINER_IDS}"
    docker rm "${EXISTING_CONTAINER_IDS}"
fi

echo -e "Starting container..."
docker run -d --env-file ${environment_file} --env SERVER_KEY="${server_key}" --restart=unless-stopped -p $container_port:80 --name "${project_key}_${environment}" $ci_registry_image
echo -e "Container started!"
